set nocompatible " be iMproved, required

" ---=== Vim-Plug Config ===---

" Download plug.vim and put it in ~/.vim/autoload
"
"   curl -fLo ~/.vim/autoload/plug.vim --create-dirs \
"     https://raw.githubusercontent.com/junegunn/vim-plug/master/plug.vim

" Specify a directory for plugins
" - For Neovim: stdpath('data') . '/plugged'
" - Avoid using standard Vim directory names like 'plugin'
call plug#begin('~/.vim/plugged')

" Make sure you use single quotes

" A Vim plugin which shows a git diff in the 'gutter' (sign column).
Plug 'airblade/vim-gitgutter'
" plugin on GitHub repo
Plug 'tpope/vim-fugitive'
" fugitive for GitHub
Plug 'tpope/vim-rhubarb'
" fugitive for Bitbucket
Plug 'tommcdo/vim-fubitive'
" fugitive for GitLab
Plug 'shumphrey/fugitive-gitlab.vim'
" plugin from http://vim-scripts.org/vim/scripts.html
" Plug 'L9'
" ASCII Art
Plug 'vim-scripts/DrawIt'
" Filesystem Tree
Plug 'scrooloose/nerdtree'
" plugin VimErl
" Plug 'jimenezrick/vimerl'
" plugin Tags Tool
" Plug 'xolox/vim-misc'
" Plug 'xolox/vim-easytags'
" Gutentags
Plug 'ludovicchabant/vim-gutentags'
" YouCompleteMe for C++
" Plug 'Valloric/YouCompleteMe'
" Dates Utility
Plug 'tpope/vim-speeddating'
" unimpaired.vim
Plug 'tpope/vim-unimpaired'
" Vim sugar for the UNIX shell commands that need it the most.
Plug 'tpope/vim-eunuch'
" plugin Vim Project
" Plug 'amiorin/vim-project'
" Git plugin not hosted on GitHub
" Plug 'git://git.wincent.com/command-t.git'
" git repos on your local machine (i.e. when working on your own plugin)
" Plug 'file:///home/gmarik/path/to/plugin'
" The sparkup vim script is in a subdirectory of this repo called vim.
" Pass the path to set the runtimepath properly.
" Sparkup lets you write HTML code faster.
" Plug 'rstacruz/sparkup', {'rtp': 'vim/'}
" Always highlights the XML/HTML tags that enclose your cursor location
Plug 'Valloric/MatchTagAlways'
" Avoid a name conflict with L9
" Plug 'user/L9', {'name': 'newL9'}
" Tag List
Plug 'vim-scripts/taglist.vim'
" Tagbar a class outline viewer for Vim
Plug 'majutsushi/tagbar', { 'commit': 'a0526a6d5a26533132acbca8c581af9affe5d04f' }
" HTML editing plugin
Plug 'mattn/emmet-vim'
" CMake plugin
Plug 'vhdirk/vim-cmake'
" Pydocstring is a generator for Python docstrings
Plug 'heavenshell/vim-pydocstring'
" generates JSDoc block comments based on a function signature
Plug 'heavenshell/vim-jsdoc'
" Vim plugin for the Perl module / CLI script 'ack'
" requires silversearcher-ag utility
Plug 'mileszs/ack.vim'
" Rooter changes the working directory to the project root when you open a file or directory.
Plug 'airblade/vim-rooter'
" Syntastic is a syntax checking plugin, requires pylint to be installed
Plug 'vim-syntastic/syntastic'
" JavaScript bundle for vim, this bundle provides syntax highlighting and improved indentation.
Plug 'pangloss/vim-javascript'
" Go support
" Plug 'fatih/vim-go'
" Plug 'fatih/vim-go', {'tag': 'v1.21', 'do': ':GoInstallBinaries' }
Plug 'fatih/vim-go', {'do': ':GoInstallBinaries' }
" aim is to integrate Direnv and Vim
" requires direnv utility
Plug 'direnv/direnv.vim'
" fzf vim
Plug 'junegunn/fzf', { 'do': 'yes \| ./install' }
Plug 'junegunn/fzf.vim'
" virtualenv.vim
Plug 'plytophogy/vim-virtualenv'
" Asynchronous Lint Engine
Plug 'w0rp/ale'
" vim-python-pep8-indent
Plug 'Vimjas/vim-python-pep8-indent'
" javascript jsx syntax
" Has indent issues
" Plug 'mxw/vim-jsx'
" Babel shows how the current file or a snippet of Javascript will be transformed by Babel.
Plug 'jbgutierrez/vim-babel'
" run shell commands in background and read output in the quickfix window in realtime
Plug 'skywind3000/asyncrun.vim'
" Indent Guides is a plugin for visually displaying indent levels in Vim.
Plug 'nathanaelkane/vim-indent-guides'
" This plugin shows how many times does a search pattern occur in the current buffer.
" Plug 'google/vim-searchindex'
" At every search command, it automatically prints> At match #N out of M matches".
Plug 'henrik/vim-indexed-search'
" awesome Python autocompletion with VIM
Plug 'davidhalter/jedi-vim'
" Better JSON for VIM
Plug 'elzr/vim-json'
" A very fast, multi-syntax context-sensitive color name highlighter
Plug 'ap/vim-css-color'
" Markdown Preview for (Neo)vim
Plug 'iamcco/markdown-preview.nvim', { 'do': { -> mkdp#util#install() } }

" Initialize plugin system
call plug#end()

" ---=== Personal visual settings ===---

" NERD Tree
" Start NERDTree if no file is specified.
autocmd StdinReadPre * let s:std_in=1
autocmd VimEnter * if argc() == 0 && !exists("s:std_in") | NERDTree | endif
"Close vim if NERDTree is the only window left open.
autocmd bufenter * if (winnr("$") == 1 && exists("b:NERDTreeType") && b:NERDTreeType == "primary") | q | endif
" Temp min menu while NERD Tree is bugged and menu breaks window size in vim 9
let g:NERDTreeMinimalMenu=1

" Automatically Quit Vim if Actual Files are Closed
" https://yous.be/2014/11/30/automatically-quit-vim-if-actual-files-are-closed/
function! CheckLeftBuffers()
  if tabpagenr('$') == 1
    let i = 1
    while i <= winnr('$')
      if getbufvar(winbufnr(i), '&buftype') == 'help' ||
          \ getbufvar(winbufnr(i), '&buftype') == 'quickfix' ||
          \ exists('t:NERDTreeBufName') &&
          \   bufname(winbufnr(i)) == t:NERDTreeBufName ||
          \ bufname(winbufnr(i)) == '__Tag_List__'
        let i += 1
      else
        break
      endif
    endwhile
    if i == winnr('$') + 1
      qall
    endif
    unlet i
  endif
endfunction
autocmd BufEnter * call CheckLeftBuffers()

let g:NERDTreeMapHelp='<f1>'

" gitgutter options
let g:gitgutter_enabled = 0
set updatetime=250 " update rate 250 ms
let g:gitgutter_max_signs = 5000

" Gutentags options
" Root is detected by tags file in dir hierarchy, force ignore by adding .notags
" exclude example ['@.gitignore']
let g:gutentags_ctags_exclude = ['venv', '_dev', '_proc']
let g:gutentags_ctags_tagfile = 'tags'
let g:gutentags_project_root = ['tags']
let gutentags_add_default_project_roots = 0
let g:gutentags_ctags_extra_args = ['--python-kinds=-iv']

" fzf options
" Enable per-command history.
" CTRL-N and CTRL-P will be automatically bound to next-history and
" previous-history instead of down and up. If you don't like the change,
" explicitly bind the keys to down and up in your $FZF_DEFAULT_OPTS.
let g:fzf_history_dir = '~/.fzf/fzf-history'
" [Tags] Command to generate tags file
let g:fzf_tags_command = 'ctags -R --python-kinds=-iv --exclude venv --exclude _dev --exclude _proc --exclude node_modules'
" Ag don't search file path, only content
command! -bang -nargs=* Ag call fzf#vim#ag(<q-args>, {'options': '--delimiter : --nth 4..'}, <bang>0)
" Page-Up, Page-Down
let $FZF_DEFAULT_OPTS .= ' --bind ctrl-f:page-down,ctrl-b:page-up,ctrl-d:half-page-down,ctrl-u:half-page-up'

" fuzzy search files in parent directory of current file
function! s:fzf_neighbouring_files()
  let current_file =expand("%")
  let cwd = fnamemodify(current_file, ':p:h')
  let command = 'ag -g "" -f ' . cwd . ' --depth 0'

  call fzf#run({
        \ 'source': command,
        \ 'sink':   'e',
        \ 'options': '-m -x +s',
        \ 'window':  'enew' })
endfunction

command! FZFNeigh call s:fzf_neighbouring_files()

" Prevents opening any file in NERDTree when NERDtree is focused
autocmd FileType nerdtree let t:nerdtree_winnr = bufwinnr('%')
autocmd BufWinEnter * call PreventBuffersInNERDTree()

function! PreventBuffersInNERDTree()
  if bufname('#') =~ 'NERD_tree' && bufname('%') !~ 'NERD_tree'
    \ && exists('t:nerdtree_winnr') && bufwinnr('%') == t:nerdtree_winnr
    \ && &buftype == '' && !exists('g:launching_fzf')
    let bufnum = bufnr('%')
    close
    exe 'b ' . bufnum
    exe "normal :NERDTreeFind\<CR>"
    exe "normal \<C-W>\<C-w>"
  endif
  if exists('g:launching_fzf') | unlet g:launching_fzf | endif
endfunction

" virtualenv.vim optins
let g:virtualenv_directory = '.'

" Ack options
if executable('ag')
  let g:ackprg = 'ag'
endif
cnoreabbrev Ack Ack!

" Pydocstring options
let g:pydocstring_enable_mapping = 0

" Vim-Rooter options
let g:rooter_manual_only = 1

" TagList options
" https://aufather.wordpress.com/2010/09/01/taglist-in-vim/
let Tlist_Close_On_Select = 1 "close taglist window once we selected something
let Tlist_Exit_OnlyWindow = 1 "if taglist window is the only window left, exit vim
let Tlist_Show_Menu = 1 "show Tags menu in gvim
let Tlist_Show_One_File = 1 "show tags of only one file
let Tlist_GainFocus_On_ToggleOpen = 1 "automatically switch to taglist window
let Tlist_Highlight_Tag_On_BufEnter = 1 "highlight current tag in taglist window
let Tlist_Process_File_Always = 1 "even without taglist window, create tags file, required for displaying tag in statusline
let Tlist_Use_Right_Window = 1 "display taglist window on the right
let Tlist_Display_Prototype = 1 "display full prototype instead of just function name
"let Tlist_Ctags_Cmd = /path/to/exuberant/ctags

nnoremap <F5> :TlistToggle <CR>
nnoremap <F6> :TlistShowPrototype <CR>

" TagBar options
nmap <F8> :TagbarToggle<CR>

" Vim-Indent Guides options
let g:indent_guides_default_mapping = 0

" emmet options
let g:user_emmet_leader_key='<C-e>'
let g:user_emmet_settings = {
\  'javascript.jsx' : {
\      'extends': 'jsx',
\  },
\}

" Essential .vimrc configuration items
let mapleader = ","  " Map leader to ,
set history=1000 " Keep a longer history
runtime macros/matchit.vim " Enable extended % matching
set wildmenu " Make file/command completion useful
set redrawtime=10000 " Syntax for large files
" set re=1 " Force the old regex engine on any version newer - speed up large files editing
set re=2 " Don't hang when open tsx files
set ttyfast " speed up large files editing
set lazyredraw " speed up large files editing

" General hotkeys
" Toggle line numbers and paste
nmap \l :setlocal number!<CR>
nmap \o :set paste!<CR>
nmap \w :setlocal wrap!<CR>:setlocal wrap?<CR>
nmap \t :GutentagsUpdate!<CR>
nmap \r :Rooter<CR>
nmap \s :Rooter<CR>:Ack -U '\b<cword>\b' -s --ignore-dir venv --ignore-dir node_modules<CR>
nmap \S :Ack -U --ignore-dir venv --ignore-dir vendor ""<left>
nmap \n :NERDTreeFind<CR>
" lint toggle
nmap \L :ALEToggle<CR>
nmap \G :GitGutterToggle<CR>
nmap \i :IndentGuidesToggle<CR>

" FZF search
nmap ,a :Ag<CR>
nmap ,c :Commands<CR>
nmap ,C <plug>(fzf-maps-n)
xmap ,C <plug>(fzf-maps-x)
omap ,C <plug>(fzf-maps-o)
nmap ,f :GFiles<CR>
nmap ,F :Files<CR>
nmap ,b :History<CR>
nmap ,B :Buffers<CR>
nmap ,t :BTags<CR>
nmap ,T :Tags<CR>
nmap ,l :BLines<CR>
nmap ,L :Lines<CR>
nmap ,n :FZFNeigh<CR>

" Speed up common commands
nnoremap <C-e> 3<C-e>
" emmet rebinds <C-y>
nnoremap <C-y><C-y> 3<C-y>
nnoremap ,, ,
nmap ,w :write<CR>
nmap ,e :edit<CR>
nmap ,P "0P
nmap ,p "0p
nmap <C-w>ts <C-w>s<C-w>T

" Tabs easy navigation
nmap <S-h> gT
nmap <S-l> gt

" Go up and down wrapped lines
nmap j gj
nmap k gk

" Shell like comand line navigation
cnoremap <C-a>  <Home>
cnoremap <C-b>  <Left>
cnoremap <C-f>  <Right>
cnoremap <C-d>  <Delete>
cnoremap <M-b>  <S-Left>
cnoremap <M-f>  <S-Right>
cnoremap <M-d>  <S-right><Delete>
cnoremap <Esc>b <S-Left>
cnoremap <Esc>f <S-Right>
cnoremap <Esc>d <S-right><Delete>
cnoremap <C-g>  <C-c>

" Cycle between all open buffers
nmap <C-n> :bnext<CR>
nmap <C-p> :bprev<CR>

" Window Navigation
map <C-j> <C-W>j
map <C-k> <C-W>k
map <C-h> <C-W>h
map <C-l> <C-W>l

" Vim autocompletion
" set completeopt=longest,menuone " popup menu doesn't select the first completion item
set completeopt=menuone
" inoremap <expr> <TAB> pumvisible() ? "\<C-y>" : "\<CR>"
inoremap <expr> <CR> pumvisible() ? "\<C-y>" : "\<C-g>u\<CR>"
" inoremap <expr> <Esc> pumvisible() ? "\<C-e>" : "\<Esc>"
inoremap <expr> <C-j> pumvisible() ? "\<C-n>" : "\<Down>"
inoremap <expr> <C-k> pumvisible() ? "\<C-p>" : "\<Up>"

autocmd FileType go nnoremap <silent> ,u :GoReferrers<CR>
autocmd FileType go nnoremap <silent> ,i :GoImplements<CR>
autocmd FileType go nnoremap <silent> ,m :GoDecls<CR>
autocmd FileType go nnoremap <silent> ,M :GoDeclsDir<CR>

autocmd FileType go nnoremap <silent> ,r :GoRun<CR>
autocmd FileType go nnoremap <silent> ,R :GoTest<CR>

autocmd FileType go nnoremap <silent> ,d :GoBuild<CR>

" Bind ,r to save file if modified and execute python script in a buffer.
" autocmd FileType python nnoremap <silent> ,r :call SaveAndExecutePython()<CR>
autocmd FileType python nnoremap <silent> ,r :AsyncRun python3 %<CR>
autocmd FileType python nnoremap <silent> ,R :AsyncRun python3 -m unittest %:r<CR>

autocmd FileType javascript nnoremap <silent> ,r :AsyncRun node --harmony %<CR>

autocmd FileType python nmap \c :Pydocstring<CR>
autocmd FileType javascript nmap \c :JsDoc<CR>

function! SaveAndExecutePython()
    " SOURCE [reusable window]: https://github.com/fatih/vim-go/blob/master/autoload/go/ui.vim

    " save and reload current file
    silent execute "update | edit"

    " get file path of current file
    let s:current_buffer_file_path = expand("%")

    let s:output_buffer_name = "Python"
    let s:output_buffer_filetype = "output"

    " reuse existing buffer window if it exists otherwise create a new one
    if !exists("s:buf_nr") || !bufexists(s:buf_nr)
        silent execute 'botright new ' . s:output_buffer_name
        let s:buf_nr = bufnr('%')
    elseif bufwinnr(s:buf_nr) == -1
        silent execute 'botright new'
        silent execute s:buf_nr . 'buffer'
    elseif bufwinnr(s:buf_nr) != bufwinnr('%')
        silent execute bufwinnr(s:buf_nr) . 'wincmd w'
    endif

    silent execute "setlocal filetype=" . s:output_buffer_filetype
    setlocal bufhidden=delete
    setlocal buftype=nofile
    setlocal noswapfile
    setlocal nobuflisted
    setlocal winfixheight
    setlocal cursorline " make it easy to distinguish
    setlocal nonumber
    setlocal norelativenumber
    setlocal showbreak=""

    " clear the buffer
    setlocal noreadonly
    setlocal modifiable
    %delete _

    " add the console output
    silent execute ".!python " . shellescape(s:current_buffer_file_path, 1)

    " resize window to content length
    " Note: This is annoying because if you print a lot of lines then your code buffer is forced to a height of one line every time you run this function.
    "       However without this line the buffer starts off as a default size and if you resize the buffer then it keeps that custom size after repeated runs of this function.
    "       But if you close the output buffer then it returns to using the default size when its recreated
    "execute 'resize' . line('$')

    " make the buffer non modifiable
    setlocal readonly
    setlocal nomodifiable
endfunction

function! CloseHiddenBuffers()
    " Tableau pour memoriser la visibilite des buffers
    let visible = {}
    " Pour chaque onglet...
    for t in range(1, tabpagenr('$'))
        " Et pour chacune de ses fenetres...
        for b in tabpagebuflist(t)
            " On indique que le buffer est visible.
            let visible[b] = 1
        endfor
    endfor
    " Pour chaque numero de buffer possible...
    for b in range(1, bufnr('$'))
        " Si b est un numero de buffer valide et qu'il n'est pas visible, on le
        " supprime.
        if bufexists(b) && !has_key(visible, b)
            " On ferme donc tous les buffers qui ne valent pas 1 dans le tableau et qui
            " sont pourtant charges en memoire.
            execute 'bwipeout' b
        endif
    endfor
endfun

function! StatuslineGit()
  let l:branchname = fugitive#head()
  return strlen(l:branchname) > 0?'  '.l:branchname.' ':''
endfunction

set statusline =
" Buffer number
set statusline +=[%n]
" Git branch (needs fugitive)
" set statusline+=%{StatuslineGit()}
" File description
" set statusline +=\ %<%.40F
set statusline +=\ %<%F
" Filetype and modes
set statusline +=\ %([%1*%M%*%R%Y]%)
" Left/right separator
set statusline+=%=
" Name of the current function (needs tagbar)
set statusline +=\ %{tagbar#currenttag('[%s]\ ','','f')}
" Total number of lines in the file, line, column and percentage
set statusline +=\ %-19(\LINE\ [%l/%L]\ COL\ [%02c]%)\ %P

set laststatus=2

let g:syntastic_always_populate_loc_list = 1
let g:syntastic_auto_loc_list = 1
" let g:syntastic_check_on_open = 1
let g:syntastic_mode_map = {'mode':'passive'}
let g:syntastic_check_on_wq = 0
let g:syntastic_python_pylint_post_args = "--max-line-length=120"
let g:syntastic_error_symbol = "✗"
let g:syntastic_warning_symbol = "⚠️"

" ALE options
" Linter Variables:
let g:ale_python_flake8_executable = 'flake8'
let g:ale_python_flake8_options = ''
let g:ale_python_flake8_use_global = 0
let g:ale_python_mypy_executable = 'mypy'
let g:ale_python_mypy_options = ''
let g:ale_python_mypy_use_global = 0
let g:ale_python_pylint_executable = 'pylint'
let g:ale_python_pylint_options = "--max-line-length=120"
let g:ale_python_pylint_use_global = 0
" Global Variables:
let g:ale_open_list = 0
let g:ale_enabled = 0
let g:ale_sign_error = "✗"
let g:ale_sign_warning = "⚠️"

" vim-jsx options
let g:jsx_ext_required = 1

" Go options
let g:go_version_warning = 0
" let g:go_fmt_options = ''
let g:go_fmt_command = 'goimports'
let g:go_test_timeout = '60s'
let g:go_auto_type_info = 0
" let g:go_info_mode = 'guru'
" let g:go_def_mode = 'go_def_mode'
" let g:go_referrers_mode = 'guru'

" DEPRICATED
" " xolox easytags options
" " Tell EasyTags to use the tags file found by Vim
" let g:easytags_dynamic_files = 1
" " let g:easytags_always_enabled = 1
" let g:easytags_include_members = 1
" let g:easytags_resolve_links = 1
" let g:easytags_auto_highlight = 0
" let g:easytags_dynamic_files = 1
" let g:easytags_async = 1
" let g:easytags_events = ['BufWritePost']
" " Use easy tags with :UpdateTags **/*

" asyncrun options
" asyncrun now has an option for opening quickfix automatically
let g:asyncrun_open = 15

" vim-indexed-search options
let g:indexed_search_colors = 0
let g:indexed_search_shortmess = 1
let g:indexed_search_numbered_only = 1

" jedi-vim options
let g:jedi#popup_on_dot = 0
let g:jedi#popup_select_first = 0
let g:jedi#show_call_signatures = 2
let g:jedi#show_call_signatures_delay = 0
let g:jedi#auto_vim_configuration = 0
let g:jedi#smart_auto_mappings = 0

let g:jedi#goto_command = "<leader>]"
let g:jedi#goto_assignments_command = "<leader>}"
let g:jedi#goto_definitions_command = ""
let g:jedi#documentation_command = "K"
let g:jedi#usages_command = "<leader>u"
let g:jedi#completions_command = ""
let g:jedi#rename_command = ""

" Omni complete
let g:omni_sql_no_default_maps = 1

" Alias for current dir path.
cabbr <expr> %$ expand('%:p:h')
cabbr <expr> %% expand('%:p:r')
"colo morning
"colo evening
colo default

if &diff
    colorscheme evening
endif

" set autochdir
autocmd BufEnter * if &ft !~ '^nerdtree$' | silent! lcd %:p:h | endif
set tags=./tags,tags;$HOME
for tagfile in split(glob('~/.vim/tags/*'), "\n")
let &tags .=',' . tagfile
endfor

set expandtab           " enter spaces when tab is pressed
set tabstop=4           " use 4 spaces to represent tab
set shiftwidth=4        " number of spaces to use for auto indent
set softtabstop=4
set autoindent          " copy indent from current line when starting a new line

autocmd BufNewFile,BufRead *.proto setfiletype proto

autocmd Filetype proto setlocal ts=2 sw=2 sts=2 expandtab
autocmd Filetype ruby setlocal ts=2 sw=2 sts=2 expandtab
autocmd FileType python setlocal tabstop=4 expandtab
autocmd FileType javascript setlocal tabstop=2 shiftwidth=2 softtabstop=2 expandtab
autocmd Filetype html setlocal tabstop=2 shiftwidth=2 softtabstop=2 expandtab
autocmd Filetype css setlocal tabstop=2 shiftwidth=2 softtabstop=2 expandtab

set backspace=indent,eol,start

filetype on
syntax on               " syntax highlighting
" Syntax coloring lines that are too long just slows down the world
" set synmaxcol=128
set ttyfast " u got a fast terminal
set ttyscroll=3
set lazyredraw " to avoid scrolling problems

set clipboard=unnamed
" Maximum amount of memory (in Kbyte) to use for pattern matching. Large value
" is needed for markdown documents.
set maxmempattern=5000

augroup vimrc
  " Automatically delete trailing DOS-returns and whitespace on file open and
  " write.
  autocmd BufRead,BufWritePre,FileWritePre * silent! %s/[\r \t]\+$//
augroup END

let g:solarized_termtrans = 1

" set mouse=a
